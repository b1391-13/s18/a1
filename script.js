let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function(){
        console.log(`${this.pokemon[0]}! I choose you!`)
    } 
}
console.log(trainer)

let accessor = {
    dotNotation: "Result of dot notation",
    bracketNotation: "Result of bracket notation"
}
console.log(accessor.dotNotation)
console.log(trainer.name)
console.log(accessor.bracketNotation)
console.log(trainer.pokemon)


function Pokemon(name, lvl, hp){
	this.name = name;
	this.level = lvl;
	this.health = hp * 2;
	this.attack = lvl;
  this.faint = false;

  this.tackle = function (target) {
    if (this.faint) {
      console.log(`${this.name} can't attack anymore`);
      return; 
    }

    console.log(`${this.name} tackled ${target.name}`);
    if (target.health <= this.attack) {
      target.faint = true;
      console.log(`${target.name} fainted`);
    } else {
			target.health = target.health - this.attack
      console.log(`${target.name}'s health is now reduced to ${target.health}`);
    }
  };
}



let Pikachu = new Pokemon("Pikachu", 5, 50);

let Geodude = new Pokemon("Geodude", 8, 40);

let Meotwo = new Pokemon ("Meotwo", 100, 100 )
console.log(Pikachu)
console.log(Geodude)	
console.log(Meotwo)


Pikachu.tackle(Geodude);
Geodude.tackle(Pikachu);
Meotwo.tackle(Geodude);


